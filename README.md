Session Version
===============

Ensures session data structure (accessed using Nette Framework) is compatible with current app version.


Usage
-----
```php
// $session is an instance of Nette\Http\Session
$session->onStart[] = [new ElektroPotkan\SessionVersion\Checker('MyApp_v4.57.2-rc1'), 'check'];
```

It is neccessary to register the `Checker` into `Session` as early as possible.
If any code tries to manipulate the session before,
the `onStart` event will be missed and the too-late-registered checker will not be called.

### Using Nette DI
```neon
services:
	- ElektroPotkan\SessionVersion\Checker('MyApp_v4.57.2-rc1')
	session.session:
		setup:
			- '$onStart[]' = [@ElektroPotkan\SessionVersion\Checker, check]
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
