.PHONY: clean phpstan


TMPDIR=/tmp/php-packages-tests/elektro-potkan/session-version


all: phpstan

clean:
	-rm tests/phpstan.local.neon
	-rm -r '$(TMPDIR)'


$(TMPDIR):
	mkdir -p '$@'

tests/phpstan.local.neon:
	echo 'includes:' > '$@'
	echo '	- phpstan.neon' >> '$@'
	echo 'parameters:' >> '$@'
	echo '	tmpDir: $(TMPDIR)/phpstan' >> '$@'


phpstan: $(TMPDIR) tests/phpstan.local.neon
	vendor/bin/phpstan analyse -c tests/phpstan.local.neon -l max src
