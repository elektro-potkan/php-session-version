<?php

declare(strict_types=1);

namespace ElektroPotkan\SessionVersion;

use Nette;
use Nette\Http\Session;


/**
 * Ensures the session is from the same app version to ensure data consistency
 */
class Checker {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $version;
	
	
	/**
	 * Constructor
	 */
	public function __construct(string $version){
		$this->version = $version;
	} // constructor
	
	/**
	 * Checks the session and clears it if not from the same app version
	 */
	public function check(Session $session): void {
		$checkSection = $session->getSection(self::class);
		
		if($checkSection->get('version') === $this->version){
			return;
		};
		
		// remove all session data
		foreach($session->getIterator() as $sectionName){// for some reason foreach($session as ...) iterates over on* event handler arrays
			$session->getSection($sectionName)->remove();
		};
		
		// init our checking section
		$checkSection->set('version', $this->version);
	} // check
} // class Checker
